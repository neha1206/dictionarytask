a="Python ia an easy language."
b="0123456789"

#indexing
print(a[-4])
print(a[-4:])

#step size
print(a[::1])
print(a[::2])
print(a[::3])

print(b[::1])
print(b[::2])
print(b[::3])
print(b[::4])
print(b[::-1])
print(b[::-2])

#functions

ab="python"
cd="language"
e=1
print(ab+cd)
print(ab*3)

#membership operator
print('t' in ab)
print('t' not in ab)

#identity operator
print(ab is "python")
print(ab is not "python")