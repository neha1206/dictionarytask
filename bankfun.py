import random
options = """
    Press 1: To create account
    Press 2: To check balance
    Press 3: To Deposit
    Press 4: To withdraw
    Press 5: To exit
"""
print(options)
users=[]
def create_account():
    name=input("Enter name: ")
    bal=int(input("enter intial amount: "))
    acc= random.randint(100,999)
    cust={"Name":name,"balance":bal,"account":acc}
    users.append(cust)
    print("Dear {} your account created successfully!!, Account no: {}".format(name,acc))

def check_bal():
    acc=int(input("enter account number: "))
    for i in users:
        if i['account']==acc:
            print("Hello",i['Name'],'!!!')
            print("Your balance, Rs: {}/-".format(i['balance']))

def deposit():
    acc=int(input("enter account number: "))
    for i in users:
        if i['account']==acc:
            amt=int(input("enter balance to deposit: "))
            i['balance']=i['balance']+amt
            print("Dear {} account credited with Rs: {}/-".format(i['Name'],amt))
            print("current balance Rs:{}/-".format(i['balance']))


while True:
    ch=input("choose an action: ")
    if ch=="1":
        create_account()
    elif ch=="2":
        check_bal()
    elif ch=="3":
        deposit()
    elif ch=="5":
        break
    else:
        print("invalid choice")
