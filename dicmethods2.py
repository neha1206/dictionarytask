emp = {
    "Name":"Mr. Abcd",
    "Age":30,
    "Salary":10000.0,
    "Emp Code":1001,
}

## Update 
print(emp["Name"])
emp["Name"] = "Mr. Lorem"
emp.update({"Name":"Harry Potter"})
print(emp["Name"])

#Insert Elements
# emp["Nme"] = "Hello There"
# print(emp)

#Delete Elements
# emp.pop("Nme")
# print(emp.popitem())
# del emp["Salary"]
# del emp
# print(emp)
