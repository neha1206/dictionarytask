#1. Positional arguments
#2. Keywords arguments
#3. Default arguments
#4. Arbitary arguments

def employee(name,salary):#(name="mr abcd",salary=0)
    print("name: {} salary: Rs: {}/-".format(name,salary))

#positional arguments
employee("peter",200000)
employee(150000,"shinchan")

#keywords arguments
employee(salary=15000,name="shinchan")
employee("peter",salary=10000)

#employee("peter",name="parker")#incorrect
#employee("harry potter")
employee("harry potter",120000)