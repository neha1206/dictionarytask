"""def palindrome(v):
    if v==v[::-1]:
        print("word is palindrome")
    else:
        print("word is not palindrome")

word=input("enter any word: ")
palindrome(word)
"""

def fun(name,email="test@domain.com"):
    print("name:",name)
    print("email:",email)
#positional arguments
fun("Neha","n@yahoo.com")
#keywords arguments
fun(email="n@yahoo.com",name="Neha")
#default arguments
fun("peter")
fun("peter","peter@gmail.com")

#multiply two number
def mul(a,b):
    print("result=",a*b)
mul(10,20)

#arbitary arguments
def mul(*args):
    print(args)
mul()
mul(10)
mul(10,20)
