for i in "python":
    print(i)

for i in range(1,11,2):
    print(i)

for i in range(1,11):
    print(i*'*')

for c in range(10,1,-1):
    print(c*'*')

str="pyhton language"
for i in str[::-1]:
    print(i)
print('\n\n\n')

for index in range(len(str)):
    print(index,':',str[index])