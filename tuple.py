ls=[20,30,40,98,90]
tp=[100,39,87,98]

#indexing/slicing/step-size
print(tp[1])
print(tp[-1])

print(tp[:2])
print(tp[:2])
print(tp[::1])
print(tp[::2])
print(tp[::3])

#basic operations
inp=("keyboard","mouse","scanner")
op=("monitor","speaker","printer")

print(inp+op)
print(inp*2)
print("keyboard" in inp)
print("keyboard" in op)

#print(ls[1])
#ls[1]=90
#print(ls[1])

#print(tp[1])
#tp[1]=9

#access elements
games=("pubg","cc","coc","ff","mario","vc","son")
for i in games:
    print(i)

for i in range(len(games)):
    print(i,'=>',games[i])

for a,b in enumerate(games):
    print(a,b)