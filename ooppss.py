"""class student:
    college="sbbsu"
    is_book_issued=False

    def intro(self,name,age):
        self.n=name
        print("Name: {} Age: {}".format(name,age,self.college))
    
    def liabrary(self,book_name):
        self.is_book_issued=True
        print("Book: {} issued by: {}".format(book_name,self.n))


#creating object
st1=student()
st2=student()

st1.intro("john",20)
st2.intro("peter",22)
st1.liabrary("python progrmming")
print("Book Issued:", st1.is_book_issued)
print("Book Issued:", st2.is_book_issued)

#accessing values
print(st1.college)
"""
class student:

    def __init__(self,name,marks):
        self.n=name
        self.m=marks
        print("values initialized")
    def hello(self):
        print("name:{} marks:{}\n".format(self.n,self.m))
    def __del__(self):
        del self.n
        del self.m
        print("destructor called")
st1=student("peter",60)
st2=student("aman",89)
st1.hello()
st2.hello()

if st1.m > st2.m:
    print(st1.n,"has highest marks")
else:
    print(st2.n,"has highest marks")
