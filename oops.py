#creating class
class employee:
#data member
    branch="SE"
    company="abcd"
#member function
    def info(self):
        print("member function")

#creating object of a class
ob=employee()

#accessing data member
print(ob.branch)
print(ob.company)

#accessing member function
ob.info()