"""def mul(*abcd):
    m=1
    for i in abcd:
        m=m*i
    print("Result: ",m)    
mul(10,20)
mul(10,20,30,0)

#arbitary number of keyword arguments

def student(**kwargs):
    for k,v in kwargs.items():
        print(k,':',v)
    print("-----------------------------")

student(email="hello@gmail.com")
student(name="shinchan",surname="nohara",age=5)
student(name="nobita",marks=0)

fields=int(input("enter number of fields: "))
dict={}
for i in range(fields):
    key=input("enter field name: ")
    val=input("enter field value: ")
    dict[key]=val
student()
"""
#lambda function
def square(a):
    return a**2
sq=lambda b:b**2
print(sq(100))

add=lambda x,y:x+y
print(add(10,45))

d=lambda st:st.isdigit()
print(d('1000'))
print(d('jhfrg'))