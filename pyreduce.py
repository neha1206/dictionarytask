from functools import reduce
marks=[100,90,80,7,33,88,22,9,79,89]
result=reduce(lambda x,y:x+y,marks)
print(result)

total=len(marks)*100
print((result/total)*100)

max=reduce(lambda a,b:a if a>b else b,marks)
print(max)

min=reduce(lambda a,b:a if a<b else b,marks)
print(min)