"""marks=[100,90,80,7,33,88,22,9,79,89]

def check(v):
    if v>33:
        return True

result=list(filter(check,marks))
print(result)
print("Total: ",len(result))
#using lamda
Result=list(filter(lambda v:(v>=33),marks))
print(Result)
print("Total: ",len(Result))

#list comprehension
comp=[i for i in range(10) if i%2==0]
#comp=[i for i in marks if i%2==0]
print(comp)
"""

ls=["aMan","hELlo","shinCHan","maRvEL","pANda","nobITA","oGGy"]
marks=[100,90,80,7,33,88,22,9,79,89]
def check(st):
    return st.title()

def perc(n):
    p=(n/100)*100
    return round(p,3)
result=list(map(check,ls))
print(result)
print(marks)
percentage=list(map(perc,marks))
print(percentage)
# USING LAMBDA
print(list(map(lambda a:a.title(),ls)))
#USING LIST COMPREHENSION
comp=[i for i in ls if i.title()]
print(comp)